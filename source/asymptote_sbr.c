/*   asymptote_sbr.c                               F. Vernotte - 1989/02/21 */
/*                            modified for fit selection by FV - 2011/01/19 */
/*                              integration into library by BD - 2023/07/10 */
/*   Subroutines for the computation of the asymptotes of a adev            */
/*   measurement serie                                                      */
/*                                                                          */
/*                                                   - SIGMA-THETA Project  */
/*                                                                          */
/* Copyright or © or Copr. Université de Franche-Comté, Besançon, France    */
/* Contributor: François Vernotte, UTINAM/OSU THETA (2012/07/17)            */
/* Contact: francois.vernotte@obs-besancon.fr                               */
/*                                                                          */
/* This software, SigmaTheta, is a collection of computer programs for      */
/* time and frequency metrology.                                            */
/*                                                                          */
/* This software is governed by the CeCILL license under French law and     */
/* abiding by the rules of distribution of free software.  You can  use,    */
/* modify and/ or redistribute the software under the terms of the CeCILL   */
/* license as circulated by CEA, CNRS and INRIA at the following URL        */
/* "http://www.cecill.info".                                                */
/*                                                                          */
/* As a counterpart to the access to the source code and  rights to copy,   */
/* modify and redistribute granted by the license, users are provided only  */
/* with a limited warranty  and the software's author,  the holder of the   */
/* economic rights,  and the successive licensors  have only  limited       */
/* liability.                                                               */
/*                                                                          */
/* In this respect, the user's attention is drawn to the risks associated   */
/* with loading,  using,  modifying and/or developing or reproducing the    */
/* software by the user in light of its specific status of free software,   */
/* that may mean  that it is complicated to manipulate,  and  that  also    */
/* therefore means  that it is reserved for developers  and  experienced    */
/* professionals having in-depth computer knowledge. Users are therefore    */
/* encouraged to load and test the software's suitability as regards their  */
/* requirements in conditions enabling the security of their systems and/or */
/* data to be ensured and,  more generally, to use and operate it in the    */
/* same conditions as regards security.                                     */
/*                                                                          */
/* The fact that you are presently reading this means that you have had     */
/* knowledge of the CeCILL license and that you accept its terms.           */
/*                                                                          */
/*                                                                          */

#include <math.h>
#include "sigma_theta.h"

#define db(x) ((double)(x))

/**
 * \brief        Resolution of a linear system with N equations and N unknowns
 *               (N<=32)
 *
 * \param np
 * \param mat
 * \param r
 * \param coeff
 * \return
 */
int resol(int np, double mat[32][32], double r[32], double coeff[10]) {
    int k, i, m, erreur, j, kp1, ieie, ip1;
    double rr, tx;

    erreur = 0;
    for (i = 0; i <= np; ++i)
        coeff[i] = db(0);
    for (k = 0; k < np; ++k) {
        i = k;
        m = k;
        do {
            i += 1;
            if (fabs(mat[i][k]) > fabs(mat[m][k]))
                m = i;
        } while (i < np);
        if (mat[m][k] == 0) {
            erreur = 1;
            break;
        }
        if (m != k) {
            for (j = k; j <= np; ++j) {
                rr = mat[k][j];
                mat[k][j] = mat[m][j];
                mat[m][j] = rr;
            }
            rr = r[k];
            r[k] = r[m];
            r[m] = rr;
        }
        kp1 = k + 1;
        for (i = kp1; i <= np; ++i) {
            rr = mat[i][k] / mat[k][k];
            mat[i][k] = 0.;
            for (j = kp1; j <= np; ++j)
                mat[i][j] -= rr * mat[k][j];
            r[i] -= rr * r[k];
        }
    }
    if (erreur == 0) {
        coeff[np] = r[np] / mat[np][np];
        for (ieie = 0; ieie < np; ++ieie) {
            i = np - ieie - 1;
            tx = 0.;
            ip1 = i + 1;
            for (j = ip1; j <= np; ++j)
                tx -= mat[i][j] * coeff[j];
            if (mat[i][i] == 0)
                erreur = 1;
            else
                coeff[i] = (r[i] + tx) / mat[i][i];
        }
    }
    return (erreur);
}

/**
 * \brief
 *
 * \param t
 * \param intyp
 * \return
 */
double st_interpo(double t, int intyp) {
    double rslt;
    switch (intyp) {
        case 0:
            rslt = db(1) / t / t / t;  // White PM: f^2 FM (only MVAR and PVAR)
            break;
        case 1:
            rslt = db(1) / t / t;  // White PM (AVAR or HVAR) of Flicker PM: f^2 or f FM
            break;
        case 2:
            rslt = db(1) / t;  // White FM: f^0 FM
            break;
        case 3:
            rslt = db(1);  // Flicker FM: f^(-1) FM
            break;
        case 4:
            rslt = t;  // Random Walk FM: f^(-2) FM
            break;
        case 5:
            rslt = t * t;  // Frequency drift (all except HVAR) or f^(-3) FM (HVAR only)
            break;
        default:
            rslt = 0;
    }
    return (rslt);
}

/**
 * \brief    Fit data using relative least square method. 
 * \details  Find the coefficients of the polynomial passing through the points M
 *           (data[0][i], data[1][i], data[2][i]) using a method derived from 
 *           the least squares method (sum of minimum relative uncertainties).
 * \param    nbm  Size of data to fit
 * \param    x    X data to fit
 * \param    y    Y data to fit
 * \param    war  Weight data  ??
 * \param    ord  Fit order  ???
 * \param    flag_slopes
 * \return   Error code
 */
int st_relatfit(int nbm, double *x, double *y, double *war, st_asymptote_coeff asym, int ord, char *flag_slopes) {
    double mat[32][32], r[32], kcr, rat, max;
    int i, j, k, m, error, indneg, indpos, cn[32];

    --nbm;
    --ord;
    for (i = 0; i < 32; ++i) {
        cn[i] = 0;
    }
    indneg = 0;
    for (i = 0; i <= ord; ++i) {
        if (!flag_slopes[i]) {
            ++indneg;
            cn[indneg] = i;
        }
    }

    /* 1) Matrix element computation */
    do {
        for (j = 0; j <= ord; ++j)
            for (m = 0; m <= ord; ++m) {
                mat[j][m] = (double)0;
                for (k = 0; k <= nbm; ++k) {
                    kcr = 1 / war[k];
                    mat[j][m] += kcr * st_interpo(x[k], j) * st_interpo(x[k], m) / y[k] / y[k];
                }
            }

        for (m = 0; m <= ord; ++m) {
            r[m] = (double)0;
            for (k = 0; k <= nbm; ++k) {
                r[m] += st_interpo(x[k], m) / y[k] / war[k];
            }
        }
        if (indneg) {
            for (i = 1; i <= indneg; ++i) {
                for (j = cn[i] - i + 1; j < ord; ++j) {
                    for (k = 0; k <= ord; ++k) {
                        mat[k][j] = mat[k][j + 1];
                    }
                    for (k = 0; k <= ord; ++k) {
                        mat[j][k] = mat[j + 1][k];
                    }
                    r[j] = r[j + 1];
                }
                --ord;
            }
        }

        /* 2) System resolution */
        error = resol(ord, mat, r, asym);
        if (error != 0)
            return -error;

        if (indneg) {
            for (i = 1; i <= indneg; ++i) {
                ++ord;
                for (j = ord; j >= cn[i]; --j)
                    asym[j + 1] = asym[j];
                asym[cn[i]] = 0;
            }
            indneg = 0;
        }
        for (i = 0; i <= ord; ++i) {
            max = db(0);
            for (j = 0; j <= nbm; ++j) {
                rat = (asym[i] * st_interpo(x[j], i)) / y[j];
                if (max < rat)
                    max = rat;
            }
            /*max=1.;*/
            if ((asym[i] <= 0) || (max < 0.15)) {
                ++indneg;
                cn[indneg] = i;
            }
        }

        indpos = 0;
        for (i = 0; i <= ord; ++i)
            if (asym[i] >= 0)
                ++indpos;
        if (indpos == ord + 1)
            indneg = 0;
        if (indpos == 0) {
            indneg = 0;
            error = 1;
        }
    } while (indneg);

    return (error);
}
