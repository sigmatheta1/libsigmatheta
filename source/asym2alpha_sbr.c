/*   asym2alpha_sbr.c                                  F. Vernotte - 2010/12/30 */
/*   Finds the dominating alpha power law noise versus tau.                 */
/*                                                                          */
/*                                                   - SIGMA-THETA Project  */
/*                                                                          */
/* Copyright or © or Copr. Université de Franche-Comté, Besançon, France    */
/* Contributor: François Vernotte, UTINAM/OSU THETA (2012/07/17)            */
/* Contact: francois.vernotte@obs-besancon.fr                               */
/*                                                                          */
/* This software, SigmaTheta, is a collection of computer programs for      */
/* time and frequency metrology.                                            */
/*                                                                          */
/* This software is governed by the CeCILL license under French law and     */
/* abiding by the rules of distribution of free software.  You can  use,    */
/* modify and/ or redistribute the software under the terms of the CeCILL   */
/* license as circulated by CEA, CNRS and INRIA at the following URL        */
/* "http://www.cecill.info".                                                */
/*                                                                          */
/* As a counterpart to the access to the source code and  rights to copy,   */
/* modify and redistribute granted by the license, users are provided only  */
/* with a limited warranty  and the software's author,  the holder of the   */
/* economic rights,  and the successive licensors  have only  limited       */
/* liability.                                                               */
/*                                                                          */
/* In this respect, the user's attention is drawn to the risks associated   */
/* with loading,  using,  modifying and/or developing or reproducing the    */
/* software by the user in light of its specific status of free software,   */
/* that may mean  that it is complicated to manipulate,  and  that  also    */
/* therefore means  that it is reserved for developers  and  experienced    */
/* professionals having in-depth computer knowledge. Users are therefore    */
/* encouraged to load and test the software's suitability as regards their  */
/* requirements in conditions enabling the security of their systems and/or */
/* data to be ensured and,  more generally, to use and operate it in the    */
/* same conditions as regards security.                                     */
/*                                                                          */
/* The fact that you are presently reading this means that you have had     */
/* knowledge of the CeCILL license and that you accept its terms.           */
/*                                                                          */
/*                                                                          */

#include "sigma_theta.h"

/**
 *
 */
int st_asym2alpha(st_serie *serie, int flag_variance) {
    int i, j;
    double tsas, asympt;

    for (i = 0; i < serie->length; ++i) {
        asympt = 0;
        if (flag_variance) {
            for (j = 0; j < 5; ++j) /* The drift is not concerned */
            {
                tsas = serie->asym[j] * st_interpo((double)serie->tau_norm[i] * serie->tau_base, j);
                if (tsas > asympt) {
                    asympt = tsas;
                    serie->alpha[i] = 2 - j;
                }
            }
        } else {
            for (j = 1; j < 5; ++j) /* Neither the tau^-3/2 asymptote nor the drift are concerned */
            {
                tsas = serie->asym[j] * st_interpo((double)serie->tau_norm[i] * serie->tau_base, j);
                if (tsas > asympt) {
                    asympt = tsas;
                    if (j == 1)
                        serie->alpha[i] = +2;
                    else
                        serie->alpha[i] = 2 - j;
                }
            }
        }
    }

    return 0;
}
