/*   psdgraph_sbr.c                               F. Vernotte - 2015/06/24  */
/*   Process the Power Spectral Density of 'ykt data'                       */
/*   i.e. normalized frequency deviation) versus the frequency              */
/*                                                                          */
/*                                                   - SIGMA-THETA Project  */
/*                                                                          */
/* Copyright or © or Copr. Université de Franche-Comté, Besançon, France    */
/* Contributor: François Vernotte, UTINAM/OSU THETA (2012/07/17)            */
/* Contact: francois.vernotte@obs-besancon.fr                               */
/*                                                                          */
/* This software, LibSigmaTheta, is a  fork of Sigma Theta                  */
/* by Benoit Dubois, dubois.benoit@gmail.com                                */
/* SigmaTheta, is a collection of computer programs for                     */
/* time and frequency metrology.                                            */
/*                                                                          */
/* This software is governed by the CeCILL license under French law and     */
/* abiding by the rules of distribution of free software.  You can  use,    */
/* modify and/ or redistribute the software under the terms of the CeCILL   */
/* license as circulated by CEA, CNRS and INRIA at the following URL        */
/* "http://www.cecill.info".                                                */
/*                                                                          */
/* As a counterpart to the access to the source code and  rights to copy,   */
/* modify and redistribute granted by the license, users are provided only  */
/* with a limited warranty  and the software's author,  the holder of the   */
/* economic rights,  and the successive licensors  have only  limited       */
/* liability.                                                               */
/*                                                                          */
/* In this respect, the user's attention is drawn to the risks associated   */
/* with loading,  using,  modifying and/or developing or reproducing the    */
/* software by the user in light of its specific status of free software,   */
/* that may mean  that it is complicated to manipulate,  and  that  also    */
/* therefore means  that it is reserved for developers  and  experienced    */
/* professionals having in-depth computer knowledge. Users are therefore    */
/* encouraged to load and test the software's suitability as regards their  */
/* requirements in conditions enabling the security of their systems and/or */
/* data to be ensured and,  more generally, to use and operate it in the    */
/* same conditions as regards security.                                     */
/*                                                                          */
/* The fact that you are presently reading this means that you have had     */
/* knowledge of the CeCILL license and that you accept its terms.           */
/*                                                                          */
/*                                                                          */

#include <gsl/gsl_fft_real.h>
#include <math.h>
#include <stdlib.h>

#include "sigma_theta.h"

#define db(x) ((double)(x))

/**
 * \brief       Power Spectral Density (PSD) calculation.
 * \details     Compute the Power Spectral Density of the 'ny' normalized
 *              frequency deviation elements of the vector '*y' (frequency deviation)
 *              versus the Fourrier frequency.
 * \param *psd  Structure containing PSD data (ff, Syy, size of data).
 * \param *t    Array of timetag data.
 * \param *y    Array of normalized frequency deviation samples.
 * \param stride Sampling time (in second)
 * \param ny    Number of elements of samples used in the computation.
 * \param idec  Decimation flag (0 no decimation else decimation).
 * \return      0 in case of successful completion.
 */
int st_psdgraph(st_psd *psd, double *y, double stride, size_t ny, int idec) {
    size_t i, j, N, M;
    double *tmp_ptr;
    double l2n, tot_dur, ksy, mulstep, logstep, klim, di;
    l2n = floor(log((double)ny) / log((double)2));
    N = (long unsigned)pow((double)2, l2n);
    // GSL make FFT computation in place (i.e. 'y' input data becomes
    // output data), but we don't want to lose 'y data'.
    // So we allocate size of 'y' for 'syy' (i.e. 'ny' elements) and
    // we copy 'y' values to 'psd->syy'.
    // 'psd->ff' only needs to allocate 'N/2' elements.
    psd->syy = (double *)malloc(ny * sizeof(*psd->syy));
    if (psd->syy == NULL) {
        return (-1);
    }
    psd->ff = (double *)malloc(N / 2 * sizeof(*psd->ff));
    if (psd->ff == NULL) {
        free(psd->syy);
        psd->syy = NULL;
        return (-1);
    }
    //
    for (i = 0; i < ny; ++i) {
        psd->syy[i] = y[i];
    }

    // Compute FFT on data
    gsl_fft_real_radix2_transform(psd->syy, 1, N);
    M = N / 2;

    // PSD estimation from FFT
    for (i = 1; i < N / 2; ++i) {
        psd->syy[i - 1] = psd->syy[i] * psd->syy[i] + psd->syy[N - i] * psd->syy[N - i];
    }
    psd->syy[N / 2 - 1] = (psd->syy[N / 2]) * (psd->syy[N / 2]);
    tot_dur = ((double)N) * stride;
    ksy = ((double)N) / (((double)2) * stride);
    for (i = 0; i < M; ++i) {
        psd->ff[i] = ((double)i + 1) / tot_dur;
        psd->syy[i] /= ksy;
    }
    psd->length = M;

    if (idec == 1)  // Data decimation
    {
        logstep = log(db(M)) / db(10000);
        mulstep = exp(logstep);
        klim = mulstep / (mulstep - db(1));

        if (klim <= M) {// TODO: test on integer or float type?
            di = klim;
            di *= mulstep;
            i = (unsigned long)di;
            j = i;
            while (i <= M) {
                psd->ff[j] = psd->ff[i];
                psd->syy[j] = psd->syy[i];
                di *= mulstep;
                i = (unsigned long)di;
                j++;
            }
            psd->length = j;
        }
    }

    // Realloc psd intensity value object to real PSD size
    tmp_ptr = NULL;
    tmp_ptr = (double *)realloc(psd->syy, (psd->length) * sizeof(*psd->syy));
    if (tmp_ptr == NULL)  // If realloc error, free pointer and exit
    {
        free(psd->syy);
        psd->syy = NULL;
        free(psd->ff);
        psd->ff = NULL;
        return (-1);
    }
    psd->syy = tmp_ptr;

    // Realloc psd frequency object to real PSD size
    tmp_ptr = NULL;
    tmp_ptr = (double *)realloc(psd->ff, (psd->length) * sizeof(*psd->ff));
    if (tmp_ptr == NULL)  // If realloc error, free pointer and exit
    {
        free(psd->ff);
        psd->ff = NULL;
        free(psd->syy);
        psd->syy = NULL;
        return (-1);
    }
    psd->ff = tmp_ptr;

    return (0);
}