/*   dev_sbr.c                                     F. Vernotte - 2014/10/02 */
/*                               From adev_sbr.c, created by FV, 2010/10/20 */
/*                               From mdev_sbr.c, created by FV, 2014/09/24 */
/*		     Adding of Parabolic deviation (PDev) by FV, 2015/02/06 */
/*       Subroutines for the computation of several Deviations (ADEV, MDev) */
/*                                                                          */
/*                                                   - SIGMA-THETA Project  */
/*                                                                          */
/* Copyright or © or Copr. Université de Franche-Comté, Besançon, France    */
/* Contributor: François Vernotte, UTINAM/OSU THETA (2012/07/17)            */
/* Contact: francois.vernotte@obs-besancon.fr                               */
/*                                                                          */
/* This software, SigmaTheta, is a collection of computer programs for      */
/* time and frequency metrology.                                            */
/*                                                                          */
/* This software is governed by the CeCILL license under French law and     */
/* abiding by the rules of distribution of free software.  you can  use,    */
/* modify and/ or redistribute the software under the terms of the CeCILL   */
/* license as circulated by CEA, CNRS and INRIA at the following URL        */
/* "http://www.cecill.info".                                                */
/*                                                                          */
/* As a counterpart to the access to the source code and  rights to copy,   */
/* modify and redistribute granted by the license, users are provided only  */
/* with a limited warranty  and the software's author,  the holder of the   */
/* economic rights,  and the successive licensors  have only  limited       */
/* liability.                                                               */
/*                                                                          */
/* In this respect, the user's attention is drawn to the risks associated   */
/* with loading,  using,  modifying and/or developing or reproducing the    */
/* software by the user in light of its specific status of free software,   */
/* that may mean  that it is complicated to manipulate,  and  that  also    */
/* therefore means  that it is reserved for developers  and  experienced    */
/* professionals having in-depth computer knowledge. Users are therefore    */
/* encouraged to load and test the software's suitability as regards their  */
/* requirements in conditions enabling the security of their systoms and/or */
/* data to be ensured and,  more generally, to use and operate it in the    */
/* same conditions as regards security.                                     */
/*                                                                          */
/* The fact that you are presently reading this means that you have had     */
/* knowledge of the CeCILL license and that you accept its terms.           */
/*                                                                          */
/*                                                                          */

#include <assert.h>
#include <math.h>
#include <stdarg.h>
#include <stdlib.h>

#include "sigma_theta.h"

#define db(x) ((double)(x))

/**
 * \brief       Allan deviation.
 * \details     Compute the Allan deviation of the 'ny' frequency deviation
 *              elements of the vector '*y' with an integration time 'tau'.
 *
 * \param  *y   Array of sample value
 * \param  tau  Integration time to be computed
 * \param  ny   Number of elements of samples used in the computation
 */
double oadev_y(double *y, unsigned long tau, size_t ny) {
    unsigned long i, nt;
    double Myd, Myf, al;

    nt = ny - 2 * tau + 1;
    Myd = Myf = db(0);
    for (i = 0; i < tau; ++i) {
        Myd += *(y + i);
        Myf += *(y + i + tau);
    }
    if (nt == 1)
        al = sqrt((double)2) * fabs(Myd - Myf) / ((double)ny);
    else {
        al = pow(Myf - Myd, ((double)2));
        for (i = 1; i < nt; ++i) {
            Myd += *(y + tau + i - 1) - *(y + i - 1);
            Myf += *(y + 2 * tau + i - 1) - *(y + tau + i - 1);
            al += pow(Myf - Myd, ((double)2));
        }
        al = sqrt(al) / (((double)tau) * sqrt((double)(2 * nt)));
    }
    return (al);
}

/**
 * \brief       Groslambert codeviation
 * \details     gcodev_y(tau,ny) : compute the Groslambert codeviation (see arXiv:1904.05849) of the 'ny' frequency deviation elements of the vectors 'y1' and 'y2' with an integration time 'tau'.
 * \param  *y   Array of sample value
 * \param  tau  Integration time
 * \param  ny   Number of elements of samples used in the computation
 */
double gcodev_y(double *y1, double *y2, unsigned long tau, size_t ny) {
    unsigned long i, nt;
    double Myd1, Myf1, Myd2, Myf2, al, bl;

    nt = ny - 2 * tau + 1;
    Myd1 = Myf1 = Myd2 = Myf2 = (double)0;
    for (i = 0; i < tau; ++i) {
        Myd1 += *(y1 + i);
        Myf1 += *(y1 + i + tau);
        Myd2 += *(y2 + i);
        Myf2 += *(y2 + i + tau);
    }
    if (nt == 1) {
        al = (Myd1 - Myf1) * (Myd2 - Myf2);
        if (al > 0)
            bl = sqrt(((double)2) * al) / ((double)ny);
        else
            bl = -sqrt(((double)-2) * al) / ((double)ny);
    } else {
        al = (Myf1 - Myd1) * (Myf2 - Myd2);
        for (i = 1; i < nt; ++i) {
            Myd1 += *(y1 + tau + i - 1) - *(y1 + i - 1);
            Myf1 += *(y1 + 2 * tau + i - 1) - *(y1 + tau + i - 1);
            Myd2 += *(y2 + tau + i - 1) - *(y2 + i - 1);
            Myf2 += *(y2 + 2 * tau + i - 1) - *(y2 + tau + i - 1);
            al += (Myf1 - Myd1) * (Myf2 - Myd2);
        }
        if (al > 0)
            bl = sqrt(al) / (((double)tau) * sqrt((double)(2 * nt)));
        else
            bl = -sqrt(-al) / (((double)tau) * sqrt((double)(2 * nt)));
    }
    //    if (isnan(bl)) printf("Myd1=%12.6le, Myf1=%12.6le, Myd2=%12.6le, Myf2=%12.6le, al=%12.6le, ny=%d, nt=%d\n",Myd1,Myf1,Myd2,Myf2,al,ny,nt);
    return (bl);
}

/**
 * \brief       Dudullized modified Allan deviation (Francois Meyer, 199?).
 * \details     Compute the modified Allan deviation of the 'ny' frequency
 *              deviation elements of the vector 'y' with an integration time
 *              'tau'.
 *
 * \param  *y   Array of sample value
 * \param  tau  Integration time to be computed
 * \param  ny   Number of elements of samples used in the computation
 */
double mdev_y(double *y, unsigned long tau, size_t ny) {
    unsigned long to2, to3m1, to2m1, tom1;
    long i, j, k, l;
    double result, moypon, coco, qto_ajto;

    moypon = 0;
    to2 = 2 * tau;
    l = 3 * (long)tau - 2;
    for (i = 0; i < (long)tau; ++i)
        moypon += db(i + 1) * (*(y + i) - *(y + l - i));
    k = (long)tau - 2;
    l = 2 * (long)tau - 1;
    for (i = (long)tau; i < l; ++i) {
        moypon += db(k) * (*(y + i));
        k -= 2;
    }

    result = moypon * moypon;

    qto_ajto = 0;

    for (j = 0; j < (long)tau; ++j)
        qto_ajto += -*(y + j + to2) - *(y + j);

    for (j = (long)tau; j < (long)to2; ++j)
        qto_ajto += 2 * (*(y + j));

    l = (long)ny - (long)tau * 3;
    to3m1 = 3 * tau - 1;
    to2m1 = 2 * tau - 1;
    tom1 = tau - 1;

    for (i = 1; i < l; ++i) {
        moypon += qto_ajto;
        result += moypon * moypon;
        qto_ajto += *(y + i - 1) + 3 * (*(y + i + to2m1) - *(y + i + tom1)) - *(y + i + to3m1);
    }

    coco = (double)tau;
    coco *= coco;
    coco *= coco;
    result /= ((double)2) * coco * ((double)(ny - 3 * tau + 1));
    return (sqrt(result));
}

/**
 * \brief       Hadamard deviation.
 * \details     Compute the Hadamard deviation of the 'ny' frequency deviation
 *              elements of the vector 'y' with an integration time 'tau'.
 *
 * \param  *y   Array of sample value
 * \param  tau  Integration time to be computed
 * \param  ny   Number of elements of samples used in the computation
 */
double hadamard_y(double *y, unsigned long tau, size_t ny) {
    unsigned long i, ifin, cpi;
    double y1, y2, y3, dy, dify, tau2;

    tau2 = db(tau) * db(tau);
    if ((tau < 1) || ((double)tau > (((double)ny) / ((double)3))))  // TODO: Right part test -> comparison with an integer or a float??
        dify = 0;
    else {
        y1 = y2 = y3 = (double)0;
        for (i = 0; i < tau; ++i) {
            y1 += (double)*(y + i);
            y2 += (double)*(y + i + tau);
            y3 += (double)*(y + i + 2 * tau);
        }
        ifin = ny - 2 * tau;
        dify = 0;
        cpi = 1;
        if ((double)tau < (double)ny / 3.0) {  // TODO: Right part test -> comparison with an integer or a float??
            for (i = tau; i < ifin; ++i) {
                ++cpi;
                dy = ((double)2) * y2 - y1 - y3;
                dify += dy * dy / tau2;
                y1 -= (double)*(y + i - tau);
                y1 += (double)*(y + i);
                y2 -= (double)*(y + i);
                y2 += (double)*(y + i + tau);
                y3 -= (double)*(y + i + tau);
                y3 += (double)*(y + i + 2 * tau);
            }
            dy = ((double)2) * y2 - y1 - y3;
            dify += dy * dy / tau2;
            dify /= (double)(cpi);
        } else {
            dify = (((double)2) * y2 - y1 - y3) * (((double)2) * y2 - y1 - y3) / tau2;
        }
        // dify /= (double)9; // Picinbono normalisation
        dify /= (double)6;  // Hadamard normalisation of Stable32 (2023-07-06)
    }
    return (sqrt(dify));
}

/**
 * \brief       Compute the Hadamard codeviation
 * \details     Compute the Hadamard codeviation (see above gcodev) of
 *              the 'ny' frequency deviation elements of the vectors 'y1' and
 *              'y2' with an integration time 'tau'.
 * \param tau
 * \param ny
 * \returns     Hadamard codeviation value
 */
double hcodev_y(double *y1, double *y2, unsigned long tau, size_t ny) {
    unsigned long i, ifin, cpi;
    double y11, y12, y13, y21, y22, y23, dy1, dy2, dify, tau2, sqrdif;

    tau2 = ((double)tau) * ((double)tau);

    if ((tau < 1) || (tau > (ny / 3)))
        sqrdif = 0;  // TODO: simply ??? -> return 0

    else {
        y11 = y12 = y13 = y21 = y22 = y23 = (double)0;
        for (i = 0; i < tau; ++i) {
            y11 += y1[i];
            y12 += y1[i + tau];
            y13 += y1[i + 2 * tau];
            y21 += y2[i];
            y22 += y2[i + tau];
            y23 += y2[i + 2 * tau];
        }
        ifin = ny - 2 * tau;
        dify = 0;
        cpi = 1;
        if ((double)tau < ((double)ny) / ((double)3)) {  // TODO: Right part test -> comparison with an integer or a float??
            for (i = tau; i < ifin; ++i) {
                ++cpi;
                dy1 = ((double)2) * y12 - y11 - y13;
                dy2 = ((double)2) * y22 - y21 - y23;
                dify += dy1 * dy2 / tau2;
                y11 -= y1[i - tau];
                y11 += y1[i];
                y12 -= y1[i];
                y12 += y1[i + tau];
                y13 -= y1[i + tau];
                y13 += y1[i + 2 * tau];
                y21 -= y2[i - tau];
                y21 += y2[i];
                y22 -= y2[i];
                y22 += y2[i + tau];
                y23 -= y2[i + tau];
                y23 += y2[i + 2 * tau];
            }
            dy1 = ((double)2) * y12 - y11 - y13;
            dy2 = ((double)2) * y22 - y21 - y23;
            dify += dy1 * dy2 / tau2;
            dify /= (double)(cpi);
        } else
            dify = (((double)2) * y12 - y11 - y13) * (((double)2) * y22 - y21 - y23) / tau2;
        dify /= (double)6;
        if (dify < 0)
            sqrdif = -sqrt(-dify);
        else
            sqrdif = sqrt(dify);
    }

    return (sqrdif);
}

/**
 * \brief       Parabolic deviation (Vernotte et al. 2015, arXiv:1506.00687).
 * \details     Compute the Parabolic deviation of the 'ny' frequency deviation
 *              elements of the vector 'y' with an integration time 'tau'.
 *
 * \param  *y   Array of sample value
 * \param  tau  Integration time to be computed
 * \param  ny   Number of elements of samples used in the computation
 */
double pdev_y(double *y, unsigned long tau, size_t ny) {
    double *H0, OV, Ovi;
    long Tm, Tp, *Tc, i;
    unsigned long dtmx, ia, ja, N, bb;

    assert(tau > 0);
    dtmx = 2 * tau - 2;

    H0 = malloc(dtmx * sizeof(double));
    Tc = malloc(dtmx * sizeof(long));

    if (tau == 1) {
        Tc[0] = 0;
        Tc[1] = 1;
        H0[0] = (double)1;
        H0[1] = (double)-1;
        bb = 2;
        N = ny - 1;
    } else {
        for (i = 0; i < (long)tau - 1; ++i) {
            Tm = -(long)tau + 1 + i;
            Tp = i + 1;
            H0[i] = -6.0 * db(Tm * (Tm + (long)tau)) / pow(db(tau), 3);
            H0[i + (long)tau - 1] = 6 * db(Tp * (Tp - (long)tau)) / pow((double)tau, 3);
            Tc[i] = Tm + (long)tau - 1;
            Tc[i + (long)tau - 1] = Tp + (long)tau - 1;
        }
        bb = 2 * tau - 2;
        N = ny - 2 * tau + 1;
    }
    OV = 0;
    for (ia = 0; ia < N; ++ia) {
        Ovi = 0;
        for (ja = 0; ja < bb; ++ja)
            Ovi += H0[ja] * *(y + Tc[ja] + ia);
        OV += Ovi * Ovi;
    }
    OV /= (double)(2 * N);
    OV = sqrt(OV);
    free(H0);
    free(Tc);
    return (OV);
}

/**
 * \brief           Compute the deviation serie of a normalized frequency
 *                  deviation sequence.
 * \details         Deviation serie (ADEV, MDEV, PDEV or HDEV) of a normalized
 *                  frequency deviation sequence from tau=tau0 (tau0=sampling
 *                  step) to tau=N*tau0/2 or N*tau0/3 (N=number of samples)
 *                  according to the variance type (dev_type) by octave
 *                  (log step: tau_n+1=2*tau_n, default setting).
 *
 * \param *serie    Pointer to st_serie data structure.
 * \param dev_typ   Deviation type to compute (ADEV, MDEV, PDEV or HDEV).
 * \param n         Number of value.
 * \param tau_base  Integration time in second.
 * \param *y        Array of sample value.
 * \param ...       Another array of sample value.
 * \return          0 in case of successful completion.
 */
int st_serie_dev(st_serie *serie, int dev_type, size_t n, double *y, ...) {
    int i;
    va_list ylist;
    double *y2;

    va_start(ylist, y);

    for (i = 0; i < serie->length; ++i) {
        switch (dev_type) {
            case MVAR:
                serie->dev[i].mean = mdev_y(y, serie->tau_norm[i], n);
                break;
            case HVAR:
                serie->dev[i].mean = hadamard_y(y, serie->tau_norm[i], n);
                break;
            case PVAR:
                serie->dev[i].mean = pdev_y(y, serie->tau_norm[i], n);
                break;
            case GVAR:
                if (i == 0) y2 = va_arg(ylist, double *);
                serie->dev[i].mean = gcodev_y(y, y2, serie->tau_norm[i], n);
                break;
            case HCVAR:
                if (i == 0) y2 = va_arg(ylist, double *);
                serie->dev[i].mean = hcodev_y(y, y2, serie->tau_norm[i], n);
                break;
            case AVAR:
            default:
                serie->dev[i].mean = oadev_y(y, serie->tau_norm[i], n);
        }
    }
    va_end(ylist);
    return (0);
}
