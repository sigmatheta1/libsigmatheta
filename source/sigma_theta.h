#ifdef cplusplus
extern "C" {
#endif

#include <stddef.h>

/**
 * \enum    st_dev_type
 * \brief   Deviation type.
 * \details Used to specify the deviation type returned used in
 * serie_dev().
 */
typedef enum {
    AVAR, /*!< Allan deviation */
    MVAR, /*!< Modified Allan deviation */
    HVAR, /*!< Parabolic deviation */
    PVAR, /*!< Hadamard deviation */
    GVAR, /*!< Grolambert covariance */
    HCVAR /*!< Hadamard covariance */
} st_dev_type;

/**
 * \enum    st_dev_bias
 * \brief   Use biased or unbiased deviation value.
 */
typedef enum {
    BIASED = 0,  /*!< Biased deviation flag */
    UNBIASED = 1 /*!< Unbiased deviation flag */
} st_flag_bias;

/**
 * \typedef  st_asymptote_coeff
 * \brief    Array containing the tau^-1, tau^-1/2, tau^0, tau^1/2 and tau
 *           asymptote coefficients of a sequence of serie deviation.
 */
typedef double st_asymptote_coeff[10];

/**
 * \struct st_conf_int
 * \brief  Deviation value data structure, with mean and unbiased values,
 *         95% (2 sigma) and 68% (1 sigma) confidence intervals values.
 */
typedef struct st_conf_int {
    double bmin2s; /*!< 95 % confidence low value */
    double bmin1s; /*!< 68 % confidence low value */
    double mean;   /*!< Mean value */
    double unb;    /*!< Unbiased value */
    double bmax1s; /*!< 68 % confidence high value */
    double bmax2s; /*!< 95 % confidence high value */
} st_conf_int;

/**
 * \struct st_serie
 */
typedef struct st_serie {
    unsigned long tau_norm[128]; /*!< Normalized Tau value */
    double tau_base;             /*!< Base Tau value */
    st_conf_int dev[128];        /*!< Deviation estimate */
    int alpha[128];              /*!< Dominating power law */
    st_asymptote_coeff asym;     /*!< Asymptotes coefficients */
    int length;                  /*!< Length of serie */
} st_serie;

/**
 * \enum    st_tau_inc_type
 * \brief   Tau incrementation type.
 * \details Used to specify the tau incrementation.
 * - DEC: pattern -> 1 2 5 10 20 50 100 200 500 ...
 * - OCT: pattern -> 1 2 4 8 16 32 64 128 256 512 ...
 * - ALL: pattern -> 1 2 3...8 9 10 20 30...80 90 100 200 300 ...
 * - LOG: Incrementation is logarithmic with a base defined by user.
 *        For example, OCT is a specific case of LOG with base 2.
 *        Note that base must be an integer.
 * - USR: the user define a pattern of maximum 10 values e.g. 1 3 7 which are
 *        used to generate a serie 'by decade' i.e.:
 *        1 3 7 10 30 70 100 300 700 1000 3000 7000 ...
 */
typedef enum {
    DEC, /*!< Increment by decade */
    OCT, /*!< Increment by octave */
    ALL, /*!< "All" incrementation */
    LOG, /*!< Logarithm increment */
    USR  /*!< User defined incrementation */
} st_tau_inc_type;

/**
 * \struct st_tau_inc_pattern
 * Tau logarithmic increment pattern structure.
 * Choose type = 'OCT' for increment by octave (default setting),
 * 'DEC' for increment by decade or 'ALL' for all Tau incrementation.
 * If 'USR' is selected, increasing values are expected, e.g. '1 2 5' means
 * that the variance will be computed for tau = 1, 2, 5, 10, 20, 50, ...
 * The number of values per decade is limited to 10.
 */
typedef struct st_tau_inc_pattern {
    int tau_pattern[10]; /*!< List of Tau value */
    int length;          /*!< Length of Tau list */
} st_tau_inc_pattern;

/**
 * \struct st_psd
 */
typedef struct st_psd {
    double *ff;    /*!< Fourrier frequency values array */
    double *syy;   /*!< PSD values array */
    size_t length; /*!< Length of array */
} st_psd;

typedef struct filter_coef {
    double hm3;
    double hm2;
    double hm1;
    double h0;
    double hp1;
    double hp2;
    double C1;
    double C0;
} filter_coef;

double oadev_y(double *y, unsigned long tau, size_t ny);
double gcodev_y(double *y1, double *y2, unsigned long tau, size_t ny);
double mdev_y(double *y, unsigned long tau, size_t ny);
double hadamard_y(double *y, unsigned long tau, size_t ny);
double hcodev_y(double *y1, double *y2, unsigned long tau, size_t ny);
double pdev_y(double *y, unsigned long tau, size_t ny);

double st_edf_greenhall(int M, int F, int J, int S, int alpha, int d);

/**
 * \brief  Compute the deviation serie of a normalized frequency
           deviation sequence.
 * \details  Deviation serie (ADEV, MDEV, PDEV or HDEV) of a normalized
 * frequency deviation sequence from tau=tau0 (tau0=sampling step)
 * to tau=N*tau0/2 or N*tau0/3 (N=number of samples) according to the variance
 * type (dev_type) by octave (log step: tau_n+1=2*tau_n, default setting).
 * \param serie     Pointer to st_serie data structure.
 * \param dev_typ   Deviation type to compute (ADEV, MDEV, PDEV or HDEV).
 * \param n         Length of array of sample value.
 * \param y         Array of sample value.
 * \return          0 in case of successful completion.
 */
int st_serie_dev(st_serie *, int, size_t, double *, ...);

/**
 * \brief    

 * \param    nbm  Size of data to fit
 * \param    x    X data to fit
 * \param    y    Y data to fit
 * \param    war  Weight data  ??
 * \param    ord  Fit order  ???
 * \param    flag_slopes
 * \return   Error code
 */
int st_relatfit(int, double *, double *, double*, st_asymptote_coeff, int, char *);

/**
 * \brief
 *
 * \param t
 * \param intyp
 * \return
 */
double st_interpo(double, int);

/**
 */
int st_asym2alpha(st_serie *, int);

/**
 * \brief  Computation of the degrees of freedom of the Allan variance
 *         estimates.
 * \details  Based on "Uncertainty of stability variances based on finite
 *           differences" by C.A.Greenhall and W.J.Riley, 35th PTTI).
 * \param serie  Pointer to st_serie data structure.
 * \param edf    Array of equivalent degree of freedom.
 * \param flag_variance  Variance type.
 * \return  0 in case of of successful completion.
 */
int st_avardof(st_serie *, double *, char);

/**
 * \brief    Computation of the degrees of freedom of the PVAR
 * \details  Computation of the degrees of freedom of the Parabolic variance
 *           estimates (based on "Responses and Degrees of Freedom of PVAR for
 *           a Continuous Power-Law PSD" by François Vernotte, Enrico Rubiola
 *           and Siyuan Chen, to be published)
 * \param serie    Pointer to st_serie data structure.
 * \param edf      Array of equivalent degree of freedom.
 * \return         0 in case of of successful completion.
 */
int st_pvardof(st_serie *, double *);

/**
 * \brief  Computes the 95 % (2 sigma) and the 68% (1 sigma)
 *         confidence intervals of a sequence of Allan Deviations.
 * \param serie  Pointer to st_serie data structure.
 * \param edf    Array of equivalent degree of freedom.
 * \return  0 in case of of successful completion.
 */
int st_aduncert(st_serie *, double *);

/**
 * \brief  Power Spectral Density (PSD) calculation.
 * \details  Compute the Power Spectral Density of the 'ny' normalized
 * frequency deviation elements of the vector '*y' (frequency deviation)
 * versus the Fourrier frequency.
 * \param psd   Structure containing PSD data (ff, Syy, size of data).
 * \param y     Array of normalized frequency deviation samples.
 * \param stride Timestep in second
 * \param ny    Number of elements of samples used in the computation.
 * \param idec  Decimation flag (0 no decimation else decimation).
 * \return
   0 in case of successful completion.
 */
int st_psdgraph(st_psd *, double *, double, size_t, int);

st_conf_int st_raylconfint(double);

typedef struct st_tchebyfit_coeff {
    double a;   /* First order coefficient of fit */
    double b;   /* Zero order coefficient of fit */
    double var; /* Variance of the residual of fit computation */
} st_tchebyfit_coeff;

/**
 * \brief  Linear fit subroutine using the first 2 Tchebytchev polynomials.
 * \param n  Length of array of sample value.
 * \param t  Array of timetag data.
 * \param y  Array of normalized frequency deviation samples.
 * \param coeff  Coefficients of the linear fit
 * \return  0 if everything goes well.
 */
int st_tchebyfit(unsigned long, double *, double *, st_tchebyfit_coeff *);

long *st_matlab_sort(double *, size_t, double *, long *, long *);
double st_pgaussdirectfin(double *, double *, double, double);
double st_pgaussdirectavecestimbr(double *, double, double, double, double,
                                  int, double);
double *st_eig(double *, size_t, double *, double *);
double st_xinvy(double *, double *, double, size_t);
int st_ci_kltgs(double mest[3], double varnoise, double ddl, int klt,
                double est[3][3], double ci[3][5], long *indices[3],
                double *montecarl[3], double *asort[3], double *pmont[3],
                double *cd, long *indix);

/**
 * \brief Subroutines for generating random noises (FV, 1989/02/21).
 * Initialization of the random number generator using /dev/urandom
 */
long stu_init_rnd();

/**
 * \brief  Generation of a sequence of 'nbr_dat' Gaussian random numbers,
 *         centered and with unity rms.
 * \param nbr_dat  Length of the sequence.
 * \param x        Array containing sequence.
 * \return  Estimator of the RMS of the computed sequence.
 */
double stu_gausseq(long unsigned int, double *);

/**
 * \brief Subroutine filtering the white noise sequence of 'nbr_dat' Gaussian
 *  data sampled with a step 'tau0' in order to obtain a sequence with
 *  a linear frequency drift and a Power Spectral Density following power laws
 * (from f^{-2} to f^{+2}) according to the entered coefficients.
 * 1994/02/02 Francois Vernotte
 */
void stu_filtreur(unsigned long, double *, double, filter_coef);

/**
 * \brief  Conversion of a frequency deviation Yk sequence into a time error
 * x(t) sequence. The arbitrarily convention x(t0) = Y0 is used.
 * At the beginning the YK are in x[], at the end the x(t) are also in x[].
 */
void stu_yk_xt(unsigned long, double *, double, double);

/**
 * \brief           Adding the suffix to the file name.
 * \param suffixe   Suffix to add to the file name
 * \param nom_brut  Base file name.
 * \return          The file name with suffix added.
 */
char *stu_mod_nom(char *, char *);

/**
 * \brief           Generate the Tau increment pattern list.
 * \param ortau     Tau increment structure.
 * \param flags     Flag structure.
 * \return          0 in case of succefull completion.
 */
int stu_gen_tau_list(st_tau_inc_pattern *, st_tau_inc_type *);  // TODO: useless? -> remove?

/**
 * \brief           Generate the real list of Tau in a serie structure.
 * \param serie     Pointer to st_serie data structure.
 * \param n         Number of data samples
 * \param dev_type  Deviation type to compute (ADEV, MDEV, PDEV or HDEV).
 * \param tau_base  Tau increment value (sampling time)
 * \param tau_inc_type Tau increment type (tau_increment_type -> int)
 * \param *pattern  Array of 10 integer:
 * - if increment of log type, the first value of the array is used as base.
 * - else, the array is used as a 'decade' pattern (see st_tau_inc_type).
 *   Use a '0' to signify the end of the pattern, e.g.:
 *   if pattern = {1, 2, 4, 0, 6, 7, 0, 0, 8, 0}
 *   the generated Tau list is in the form 1 2 4 10 20 40 100 200 400 1000 ...
 *   The value serie must be growing else error is returned.
 *   Each value must in the range [1 - 9] else error is returned.
 * \return  0 in case of succefull completion.
 *          -1 if error in *pattern syntax
 */
int stu_populate_tau_list(st_serie *, size_t, int, double, int, int *);

/**
 * \brief  Return a scaling factor as a double defined according to
 *         the code value.
 * \param code  Code value defining a scale factor.
 * \return      Scaling factor
 */
double stu_scale(char);

/**
 * \brief      Load the file pointed by 'source' and transfer its content
 *             into the 'y' tables
 *
 * \details    File reading switched from fscanf to readline and sscanf
 *             reading only checks that there are at least 2 readable columns
 *             the existence of additional columns is not checked, and hence
 *             no longer considered a fatal error
 *
 *             Scaling can be done using chars scaley
 *
 *             if those are 0 (decimal value 0, not char '0'), no scaling
 *             applies ; see double stio_scale(char) for details.
 *
 *             uncert is 0 by default ; if not 0, a third column is read
 *             as uncertainty.
 *
 * \param *source  Path to data file.
 * \param **y      Pointer to array of sample value.
 * \param **u      Pointer to array of uncertainties value.
 * \param scaley   'y' (sample) data scaling.
 * \param uncert   If not 0, a third column is read as uncertainty.
 *
 * \return     -1 file not found,
 *             0 unrecognized file,
 *             N length of the tables.
 */
long stio_load_yk(char *, double **, double **, char, int);

/**
 * \brief      Load the file pointed by 'source' and transfer its content
 *             into the 't' and 'y' tables.
 *
 * \details    File reading switched from fscanf to readline and sscanf
 * reading only checks that there are at least 2 readable columns
 * the existence of additional columns is not checked, and hence no longer
 * considered a fatal error.
 *
 * Scaling can be done using 2 chars scalex scaley to deal with timestamps
 * available in MJD, hours or minutes and data available in ms, us, ns, ps,
 * fs.
 *
 * if those are 0 (decimal value 0, not char '0'), no scaling applies;
 * see double scale(char) for details.
 *
 * uncert is 0 by default ; if not 0, a third column is read as uncertainty.
 *
 * \param *source  Path to data file.
 * \param **t      Pointer to array of timetag.
 * \param **y      Pointer to array of sample value.
 * \param **u      Pointer to array of uncertainties value.
 * \param scalex   'x' (timetag) data scaling.
 * \param scaley   'y' (sample) data scaling.
 * \param uncert   If not 0, a third column is read as uncertainty.
 * \return         -1 file not found,
 *                 0 unrecognized file,
 *                 N length of the tables.
 */
long stio_load_ykt(char *, double **, double **, double **, char, char, int);

/**
 * \brief  Load the two files pointed by 'source1' and 'source2' and transfer
 *         its content into the 't' and 'y' tables.
 * \param source1  Path to data file 1.
 * \param source2  Path to data file 2.
 * \param t   Pointer to array of timetag.
 * \param y1  Pointer to array of sample value.
 * \param y2  Pointer to array of sample value.
 * \return -1 file not found,
 *         0 unrecognized file,
 *         N length of the tables.
 */
long stio_load_2yk(char *, char *, double **, double **, double **);

/**
 * \brief  Load the three files pointed by 'source1', 'source2' and 'source3'
 * and transfer its content into the 't', 'y1', 'y2' and 'y3' tables.
 * \param source1  Path to data file 1.
 * \param source2  Path to data file 2.
 * \param source3  Path to data file 3.
 * \param t    Pointer to array of timetag.
 * \param y12  Pointer to array of sample value from source1.
 * \param y23  Pointer to array of sample value from source2.
 * \param y31  Pointer to array of sample value from source3.
 * \return  -1 file not found,
 *          0 unrecognized file,
 *          N length of the tables.
 */
long stio_load_3yk(char *, char *, char *, double **, double **, double **,
                   double **);

/**
 * \brief  Load the two files pointed by 'source' and transfer its content
 *         into the 'y' table.
 * \param source  Path to data file.
 * \param y       Pointer to array of sample value.
 * \return  -2 file with 2 columns
 *          -1 file not found,
 *          0 unrecognized file,
 *          N length of the tables.
 */
long stio_load_1col(char *, double **);

/**
 * \brief  Load the two files pointed by 'source' and transfer its content
 *         into the 'y1' and 'y2' tables.
 * \param source  Path to data file.
 * \param y1      Array container for the first column of sample value.
 * \param y2      Array container for the second column of sample value.
 * \return        -2 file with 2 columns
 *                -1 file not found,
 *                0 Memory allocation error,
 *                N length of the tables.
 */
long stio_load_2col(char *, double [128], double [128]);

/**
 * \brief Load the file pointed by 'source' and transfer its content into
 *        the serie structure.
 * \param source  Path to data file.
 * \param serie   Pointer to st_serie object.
 * \return  -1 file not found
 *          0 unrecognized file
 *          N length of the tables
 */
int stio_load_adev(char *source, st_serie *serie);  // double tau[], double adev[]);

/**
 * \brief  Load the file pointed by 'source' and transfer its
 *         contain into the 'coeff' tables.
 * \param source  Path to data file.
 * \param coef    Struture of asymptote coefficients.
 * \return  -1 file not found
 *          0 unrecognized file
 *          N length of the tables
 */
int stio_load_coef(char *, st_asymptote_coeff);

/**
 * \brief Load the file pointed by 'source' and transfer its contain into
 *        the serie structure.
 * \param source  Path to data file.
 * \param serie
 * \return  -1 file not found
 *          0 unrecognized file
 *          N length of the tables
 */
int stio_load_adev_3col(char *, st_serie *);

/**
 * \brief Load the file pointed by 'source' and transfer its contain into
 *        the serie structure.
 * \param source  Path to data file.
 * \param serie
 * \return  -1 file not found
 *          0 unrecognized file
 *          N length of the tables
 */
int stio_load_adev_7col(char *, st_serie *);

/**
 * PRIVATE
 */

double cdf_rayleigh(double, double);
double dcdfr(double, double);

#ifdef cplusplus
}
#endif
