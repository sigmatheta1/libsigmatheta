/*   tchebyfit.c                                  F. Vernotte - 2000/06/08  */
/*   Linear fit subroutines using the first 2 Tchebytchev polynomials       */
/*                                                                          */
/*                                                   - SIGMA-THETA Project  */
/*                                                                          */
/* Copyright or © or Copr. Université de Franche-Comté, Besançon, France    */
/* Contributor: François Vernotte, UTINAM/OSU THETA (2012/07/17)            */
/* Contact: francois.vernotte@obs-besancon.fr                               */
/*                                                                          */
/* This software, SigmaTheta, is a collection of computer programs for      */
/* time and frequency metrology.                                            */
/*                                                                          */
/* This software is governed by the CeCILL license under French law and     */
/* abiding by the rules of distribution of free software.  You can  use,    */
/* modify and/ or redistribute the software under the terms of the CeCILL   */
/* license as circulated by CEA, CNRS and INRIA at the following URL        */
/* "http://www.cecill.info".                                                */
/*                                                                          */
/* As a counterpart to the access to the source code and  rights to copy,   */
/* modify and redistribute granted by the license, users are provided only  */
/* with a limited warranty  and the software's author,  the holder of the   */
/* economic rights,  and the successive licensors  have only  limited       */
/* liability.                                                               */
/*                                                                          */
/* In this respect, the user's attention is drawn to the risks associated   */
/* with loading,  using,  modifying and/or developing or reproducing the    */
/* software by the user in light of its specific status of free software,   */
/* that may mean  that it is complicated to manipulate,  and  that  also    */
/* therefore means  that it is reserved for developers  and  experienced    */
/* professionals having in-depth computer knowledge. Users are therefore    */
/* encouraged to load and test the software's suitability as regards their  */
/* requirements in conditions enabling the security of their systems and/or */
/* data to be ensured and,  more generally, to use and operate it in the    */
/* same conditions as regards security.                                     */
/*                                                                          */
/* The fact that you are presently reading this means that you have had     */
/* knowledge of the CeCILL license and that you accept its terms.           */
/*                                                                          */
/*                                                                          */

#include <math.h>

#include "sigma_theta.h"

#define db(x) ((double)(x))

double (*phi[2])(unsigned long, unsigned long);

double tcheby0(unsigned long k, unsigned long n) {
    double result;
    (void)k;
    result = db(1) / sqrt(db(n));
    return (result);
}

double tcheby1(unsigned long k, unsigned long n) {
    double result;
    result = db(2) * db(k) - (db(n) - db(1));
    result *= sqrt(db(3) / ((db(n) - db(1)) * db(n) * (db(n) + db(1))));
    return (result);
}

void initcheb(void) {
    phi[0] = tcheby0;
    phi[1] = tcheby1;
}

double extrapol(unsigned long k, unsigned long n, double *coeff) {
    int i;
    double result;

    result = db(0);
    for (i = 0; i < 2; ++i)
        result += coeff[i] * phi[i](k, n);
    return (result);
}

/**
 * \brief Linear fit subroutines using the first 2 Tchebytchev polynomials.
 *
 * \param n        Size of data set.
 * \param *t       Array of timetag data.
 * \param *y       Array of normalized frequency deviation samples.
 * \param *tcoeff  Pointer to the structure of coefficients of the linear fit.
 * \return         0 in case of sucess.
 */
int st_tchebyfit(unsigned long n, double *t, double *y, st_tchebyfit_coeff *tcoeff) {
    size_t i, j;
    double moy, var, coco, dx, dy, a, b;
    double coeff[2];

    initcheb();
    for (i = 0; i < 2; ++i) /* Computation of the most probable coefficients */
    {                       /* by the least square method */
        coeff[i] = db(0);
        for (j = 0; j < n; ++j)
            coeff[i] += phi[i](j, n) * (*(y + j));
    }
    var = moy = db(0);
    for (j = 0; j < n; ++j) /* Computation of the residuals */
        moy += *(y + j) = *(y + j) - extrapol(j, n, coeff);
    moy /= db(n);
    for (i = 0; i < n; ++i) /* Computation of the variance of the residuals */
    {
        coco = *(y + i) - moy;
        coco *= coco;
        var += coco;
    }
    var /= db(n - (long)1);
    dx = (*(t + n - 1)) - (*t);
    dy = extrapol(n - 1, n, coeff) - extrapol(0, n, coeff);
    a = dy / dx;
    b = extrapol(0, n, coeff) - a * (*t);
    tcoeff->var = var;
    tcoeff->b = b;
    tcoeff->a = a;
    return (0);
}
