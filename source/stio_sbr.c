/*   stio_sbr.c                                   F. Vernotte - 2010/10/31  */
/*   Input/output subroutines                                               */
/*                                                                          */
/*                                                   - SIGMA-THETA Project  */
/*                                                                          */
/* Copyright or © or Copr. Université de Franche-Comté, Besançon, France    */
/* Contributor: François Vernotte, UTINAM/OSU THETA (2012/07/17)            */
/* Contact: francois.vernotte@obs-besancon.fr                               */
/*                                                                          */
/* This software, SigmaTheta, is a collection of computer programs for      */
/* time and frequency metrology.                                            */
/*                                                                          */
/* This software is governed by the CeCILL license under French law and     */
/* abiding by the rules of distribution of free software.  You can  use,    */
/* modify and/ or redistribute the software under the terms of the CeCILL   */
/* license as circulated by CEA, CNRS and INRIA at the following URL        */
/* "http://www.cecill.info".                                                */
/*                                                                          */
/* As a counterpart to the access to the source code and  rights to copy,   */
/* modify and redistribute granted by the license, users are provided only  */
/* with a limited warranty  and the software's author,  the holder of the   */
/* economic rights,  and the successive licensors  have only  limited       */
/* liability.                                                               */
/*                                                                          */
/* In this respect, the user's attention is drawn to the risks associated   */
/* with loading,  using,  modifying and/or developing or reproducing the    */
/* software by the user in light of its specific status of free software,   */
/* that may mean  that it is complicated to manipulate,  and  that  also    */
/* therefore means  that it is reserved for developers  and  experienced    */
/* professionals having in-depth computer knowledge. Users are therefore    */
/* encouraged to load and test the software's suitability as regards their  */
/* requirements in conditions enabling the security of their systems and/or */
/* data to be ensured and,  more generally, to use and operate it in the    */
/* same conditions as regards security.                                     */
/*                                                                          */
/* The fact that you are presently reading this means that you have had     */
/* knowledge of the CeCILL license and that you accept its terms.           */
/*                                                                          */
/*                                                                          */

#include <assert.h>
#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "sigma_theta.h"

#define DATAMAX 16384
#define GRANMAX 67108864

/**
 * \brief           Generate the Tau increment pattern list.
 * \param ortau     Tau increment structure pattern.
 * \return          0 in case of succefull completion.
 */
int stu_gen_tau_inc_pattern(st_tau_inc_pattern *ortau, st_tau_inc_type type) {
    switch (type) {
        case OCT: {
            int tau_list[] = {0, 1, 2, 4, 8};
            ortau->length = 5;
            for (int i = 0; i < ortau->length; i++)
                ortau->tau_pattern[i] = tau_list[i];
            break;
        }
        case DEC: {
            int tau_list[] = {0, 1, 2, 5};
            ortau->length = 4;
            for (int i = 0; i < ortau->length; i++)
                ortau->tau_pattern[i] = tau_list[i];
            break;
        }
        case ALL: {
            int tau_list[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
            ortau->length = 10;
            for (int i = 0; i < ortau->length; i++)
                ortau->tau_pattern[i] = tau_list[i];
            break;
        }
        case USR: {
            // ortau.tau shoud be initialized by user instead of call to this
            // function, so nothing to do.
            break;
        }
        default: {
            return -1;
        }
    }
    return 0;
}

/**
 * TODO: move function in utils.c??
 * \brief           Generate the real list of Tau.
 * \param *serie    Pointer to st_serie data structure.
 * \param n         Number of data samples
 * \param dev_type  Deviation type to compute (ADEV, MDEV, PDEV or HDEV).
 * \param tau_inc_type Tau increment type (tau_increment_type -> int)
 * \param *pattern  Array of 10 integer:
 * - if increment of log type, the first value of the array is used as base.
 * - else, the array is used as a 'decade' pattern (see st_tau_inc_type).
 *   Use a '0' to signify the end of the pattern, e.g.:
 *   if pattern = {1, 2, 4, 0, 6, 7, 0, 0, 8, 0}
 *   the generated Tau list is in the form 1 2 4 10 20 40 100 200 400 1000 ...
 *   The value serie must be growing else error is returned.
 *   Each value must in the range [1 - 9] else error is returned.
 * \return  0 in case of succefull completion.
 *          -1 if error in *pattern syntax
 */
int stu_populate_tau_list(st_serie *serie, size_t n, int dev_type,
                          double tau_base, int tau_inc_type, int *pattern) {
    // TODO: Check dev_type range, tau_inc_type range and tau_base > 0
    assert(dev_type >= 0);
    assert(tau_inc_type >= 0);
    assert(tau_base > 0);

    int log_base;
    unsigned int decade;
    unsigned long tau_norm_max;
    unsigned long tau_test = 0;
    int serie_idx;

    serie->tau_base = tau_base;

    if ((dev_type == MVAR) || (dev_type == HVAR) || (dev_type == HCVAR))
        tau_norm_max = (long unsigned)floor((double)(n / 3));
    else
        tau_norm_max = (long unsigned)floor((double)(n / 2));

    // Generation of Tau serie
    serie_idx = 0;
    decade = 0;

    if ((tau_inc_type == OCT) || (tau_inc_type == LOG)) {
        if (tau_inc_type == OCT)
            log_base = 2;
        else
            log_base = pattern[0];
        assert(log_base > 0);
        do {
            serie->tau_norm[serie_idx] = (long unsigned)pow(log_base, (double)serie_idx);
            serie_idx++;
        } while (serie->tau_norm[serie_idx - 1] < tau_norm_max);
    } else {
        assert(pattern != NULL);
        if (tau_inc_type == DEC) {
            pattern[0] = 1;
            pattern[1] = 2;
            pattern[2] = 5;
            pattern[3] = 0;
        } else if (tau_inc_type == ALL) {
            for (int j = 0; j < 9; ++j)
                pattern[j] = j + 1;
        } else  // i.e. tau_inc_type == USR
        {
            int pvalue = 0;
            for (int j = 0; j < 9; ++j) {
                if (pattern[j] == 0)
                    break;
                if (pattern[j] < 0)
                    return -1;
                if (pattern[j] < pvalue)
                    return -1;
                pvalue = pattern[j];
            }
        }

        do {
            for (unsigned long j = 0; j <= 9; ++j) {
                if (pattern[j] == 0)
                    break;
                tau_test = (long unsigned)pattern[j] * (long unsigned)pow(10.0, (double)decade);
                if (tau_test > tau_norm_max) {
                    serie->tau_norm[serie_idx] = tau_norm_max;
                    serie_idx++;
                    break;
                }
                serie->tau_norm[serie_idx] = tau_test;
                serie_idx++;
            }
            decade += 1;
        } while (tau_test < tau_norm_max);
    }

    if (serie->tau_norm[serie_idx - 1] > tau_norm_max) {
        serie->tau_norm[serie_idx - 1] = tau_norm_max;
    }

    serie->length = serie_idx;

    return 0;
}

/**
 * \brief       Returns a scaling factor as a double defined according to
 *              the code value.
 *
 * \param code  Code value defining a stio_scale factor.
 *
 * \return      Scaling factor
 */
double stu_scale(char code) {
    double factor;

    switch (code) {
        case 'd':
            //
            // d for days, 86400 s ; useful for timestamps in MJDs
            //
            factor = 86400.;
            break;
        case 'H':
        case 'h':
            //
            // H for hours, 3600 s ; useful for timestamps in hours
            //
            factor = 3600.;
            break;
        case 'M':
            //
            // M for minutes, 60 s ; useful for timestamps in minutes
            //
            factor = 60.;
            break;
        case 'm':
            //
            // m for milliseconds ; useful for data in milliseconds
            //
            factor = 1.e-3;
            break;
        case 'u':
            //
            // u for microseconds ; useful for data in microseconds
            //
            factor = 1.e-6;
            break;
        case 'n':
            //
            // n for nanoseconds ; useful for data in nanoseconds
            //
            factor = 1.e-9;
            break;
            //
        case 'p':
            //
            // p for picoseconds ; useful for data in picoseconds
            factor = 1.e-12;
            break;
        case 'f':
            //
            // f for femtoseconds ; useful for data in femtoseconds
            //
            factor = 1.e-15;
            break;
        case 'a':
            //
            // a for attoseconds ; useful for data in attoseconds. Why not ?..
            //
            factor = 1.e-18;
            break;
        default:
            // default: no scaling.
            //    No scaling is normally dealt with code = 0
            //    so no multiplication by 1 of a full dataset
            //    should ever occur under normal use.
            factor = 1;
    }

    return factor;
}

/**
 * \brief      Load the file pointed by 'source' and transfer its content
 *             into the 'y' tables
 *
 * \details    File reading switched from fscanf to readline and sscanf
 *             reading only checks that there are at least 2 readable columns
 *             the existence of additional columns is not checked, and hence
 *             no longer considered a fatal error
 *
 *             Scaling can be done using chars scaley
 *
 *             if those are 0 (decimal value 0, not char '0'), no scaling
 *             applies ; see double stio_scale(char) for details.
 *
 *             uncert is 0 by default ; if not 0, a third column is read
 *             as uncertainty.
 *
 * \param *source  Path to data file.
 * \param **y      Pointer to array of sample value.
 * \param **u      Pointer to array of uncertainties value.
 * \param scaley   'y' (sample) data scaling.
 * \param uncert   If not 0, a third column is read as uncertainty.
 *
 * \return     -1 file not found,
 *             0 unrecognized file,
 *             N length of the tables.
 */
long stio_load_yk(char *source, double **y, double **u, char scaley, int uncert) {
    long i, N, dtmx, linecount = 0, valcount = 0;
    size_t n;
    double yscale = 1;
    uint8_t columnnb = 2;
    char *line = NULL;
    FILE *ofd;
    dtmx = DATAMAX;
    *y = (double *)malloc((unsigned long)dtmx * sizeof(**y));

    if (uncert != 0) {
        *u = (double *)malloc((unsigned long)dtmx * sizeof(**u));
        columnnb = 3;
    }

    if (scaley != 0)
        yscale = stu_scale(scaley);

    if (strlen(source) == 0 || (strlen(source) == 1 && source[0] == '-'))
        ofd = stdin;
    else
        ofd = fopen(source, "r");
    if (ofd == NULL) {
        fprintf(stderr, "Could not open file %s", source);
        return (-1);
    } else {
        i = 0;
        while (getline(&line, &n, ofd) != -1) {
            // Read an entire line
            linecount++;
            if (line[0] == '#' || line[0] == '%')
                // Ignore comment lines
                continue;
            if (uncert == 0) {
                valcount = sscanf(line, "%*f %lf", *y + i);
            } else {
                valcount = sscanf(line, "%*f %lf %lf",
                                  *y + i, *u + i);
            }
            if (valcount == columnnb) {
                // Reads 1 value out of line or 2 if uncert !=0
                // Proceed to rescaling if needed (scaley != 0)
                if (scaley)
                    (*y)[i] *= yscale;
                if (uncert)
                    (*y)[i] *= yscale;
                i++;
                if (i >= dtmx) {
                    dtmx += DATAMAX;
                    if (dtmx > GRANMAX) {
                        printf(
                            "# File truncated to %ld "
                            "elements\n",
                            dtmx);
                        break;
                    }
                    *y = realloc(*y, (unsigned long)dtmx * sizeof(**y));
                    if (uncert != 0)
                        *u = realloc(*u, (unsigned long)dtmx * sizeof(**u));
                }
            } else {
                fprintf(stderr,
                        " # ignoring line %ld : only %ld "
                        "value(s) read #%s# \n",
                        linecount, valcount, line);
            }
        }

        if (ofd != stdin)
            fclose(ofd);
        N = i;
    }
    return (N);
}

/**
 * \brief      Load the file pointed by 'source' and transfer its content
 *             into the 't' and 'y' tables.
 *
 * \details    File reading switched from fscanf to readline and sscanf
 *             reading only checks that there are at least 2 readable columns
 *             the existence of additional columns is not checked, and hence
 *             no longer considered a fatal error
 *
 *             Scaling can be done using 2 chars scalex scaley
 *             to deal with timestamps available in MJD, hours or minutes
 *             and data available in ms, us, ns, ps, fs
 *
 *             if those are 0 (decimal value 0, not char '0'), no scaling
 *             applies ; see double stio_scale(char) for details.
 *
 *             uncert is 0 by default ; if not 0, a third column is read
 *             as uncertainty.
 *
 * \param *source  Path to data file.
 * \param **t      Pointer to array of timetag.
 * \param **y      Pointer to array of sample value.
 * \param **u      Pointer to array of uncertainties value.
 * \param scalex   'x' (timetag) data scaling.
 * \param scaley   'y' (sample) data scaling.
 * \param uncert   If not 0, a third column is read as uncertainty.
 *
 * \return     -1 file not found,
 *             0 unrecognized file,
 *             N length of the tables.
 */
long stio_load_ykt(char *source, double **t, double **y, double **u, char scalex, char scaley, int uncert) {
    long i, N, dtmx, linecount = 0, valcount = 0;
    size_t n;
    double
        xscale = 1,
        yscale = 1;
    uint8_t nbvaltoread = 2;
    char *line = NULL;
    FILE *ofd;
    dtmx = DATAMAX;
    *t = (double *)malloc((unsigned long)dtmx * sizeof(**t));
    *y = (double *)malloc((unsigned long)dtmx * sizeof(**y));

    if (uncert != 0) {
        *u = (double *)malloc((unsigned long)dtmx * sizeof(**u));
        nbvaltoread = 3;
    }

    if (scalex != 0)
        xscale = stu_scale(scalex);
    if (scaley != 0)
        yscale = stu_scale(scaley);

    if (strlen(source) == 0 || (strlen(source) == 1 && source[0] == '-'))
        ofd = stdin;
    else
        ofd = fopen(source, "r");
    if (ofd == NULL) {
        fprintf(stderr, "Could not open file %s", source);
        return (-1);
    } else {
        i = 0;
        while (getline(&line, &n, ofd) != -1) {
            // Read an entire line
            linecount++;
            if (line[0] == '#' || line[0] == '%')
                // Ignore comment lines
                continue;
            if (uncert == 0) {
                valcount = sscanf(line, "%lf %lf", *t + i, *y + i);
            } else {
                valcount = sscanf(line, "%lf %lf %lf", *t + i,
                                  *y + i, *u + i);
            }
            if (valcount == nbvaltoread) {
                // Reads 2 values out of line or 3 if uncert !=0
                // Proceed to rescaling if needed (scalex != 0 or
                // scaley != 0)
                if (scalex)
                    (*t)[i] *= xscale;
                if (scaley)
                    (*y)[i] *= yscale;
                if (uncert)
                    (*y)[i] *= yscale;
                i++;
                if (i >= dtmx) {
                    dtmx += DATAMAX;
                    if (dtmx > GRANMAX) {
                        printf(
                            "# File truncated to %ld "
                            "elements\n",
                            dtmx);
                        break;
                    }
                    *t = realloc(*t, (unsigned long)dtmx * sizeof(**t));
                    *y = realloc(*y, (unsigned long)dtmx * sizeof(**y));
                    if (uncert != 0)
                        *u = realloc(*u, (unsigned long)dtmx * sizeof(**u));
                }
            } else {
                fprintf(stderr,
                        " # ignoring line %ld : only %ld "
                        "value(s) read #%s# \n",
                        linecount, valcount, line);
            }
        }

        if (ofd != stdin)
            fclose(ofd);
        N = i;
    }
    return (N);
}

/**
 * \brief           Load the two files pointed by 'source1' and 'source2' and
 *                  transfer its content into the 't' and 'y' tables.
 * \param *source1  Path to data file.
 * \param *source2  Path to data file.
 * \param **t       Pointer to array of timetag.
 * \param **y1      Pointer to array of sample value.
 * \param **y2      Pointer to array of sample value.
 * \return          -1 file not found,
 *                  0 unrecognized file,
 *                  N length of the tables.
 */
long stio_load_2yk(char *source1, char *source2, double **t, double **y1, double **y2) {
    long i, nbv, n1, n2;
    long N;
    long int dtmx;
    double tst;
    char gm[256];
    FILE *ofd, *of2;

    dtmx = DATAMAX;

    *t = (double *)malloc((unsigned long)dtmx * sizeof(**t));
    *y1 = (double *)malloc((unsigned long)dtmx * sizeof(**y1));
    *y2 = (double *)malloc((unsigned long)dtmx * sizeof(**y2));

    ofd = fopen(source1, "r");
    if (ofd == NULL)
        return (-1);
    else {
        do
            fgets(gm, 100, ofd);
        while ((gm[0] == '#') || (gm[0] == '%'));
        i = 0;
        nbv = sscanf(gm, "%lf %lf %lf", *t + i, *y1 + i, &tst);
        if (nbv != 2) {
            if (nbv != 1)
                nbv = -nbv;
            return (nbv);
        } else {
            do {
                i++;
                if (i >= dtmx) {
                    dtmx += DATAMAX;
                    if (dtmx > GRANMAX) {
                        printf(
                            "# File trucated to %ld "
                            "elements\n",
                            dtmx);
                        break;
                    }
                    *t = (double *)realloc(
                        *t,
                        (unsigned long)dtmx * sizeof(**t));
                    *y1 = (double *)realloc(
                        *y1,
                        (unsigned long)dtmx * sizeof(**y1));
                }
            } while (fscanf(ofd, "%lf %lf", *t + i, *y1 + i) == 2);
            fclose(ofd);
            n1 = i;
        }
    }

    dtmx = DATAMAX;
    of2 = fopen(source2, "r");
    if (of2 == NULL)
        return (-1);
    else {
        do
            fgets(gm, 100, of2);
        while ((gm[0] == '#') || (gm[0] == '%'));
        i = 0;
        nbv = sscanf(gm, "%lf %lf %lf", *t + i, *y2 + i, &tst);
        if (nbv != 2) {
            if (nbv != 1)
                nbv = -nbv;
            return (nbv);
        } else {
            do {
                i++;
                if (i >= dtmx) {
                    dtmx += DATAMAX;
                    if (dtmx > GRANMAX) {
                        printf(
                            "# File trucated to %ld "
                            "elements\n",
                            dtmx);
                        break;
                    }
                    *y2 = (double *)realloc(
                        *y2,
                        (unsigned long)dtmx * sizeof(**y2));
                }
            } while (fscanf(of2, "%lf %lf", *t + i, *y2 + i) == 2);
            fclose(of2);
            n2 = i;
        }
    }
    if (n1 == n2)
        N = n1;
    else
        N = -2;
    return (N);
}

/**
 * \brief           Load the two files pointed by 'source1' and 'source2' and
 *                  transfer its content into the 't' and 'y' tables.
 * \param *source1  Path to data file 1.
 * \param *source2  Path to data file 2.
 * \param *source3  Path to data file 3.
 * \param **t       Pointer to array of timetag.
 * \param **y12     Pointer to array of sample value.
 * \param **y23     Pointer to array of sample value.
 * \param **y31     Pointer to array of sample value.
 * \return          -1 file not found,
 *                  0 unrecognized file,
 *                  N length of the tables.
 */
long stio_load_3yk(char *source1, char *source2, char *source3, double **t, double **y12, double **y23, double **y31) {
    long i, nbv, n1, n2, n3;
    long N;
    long int dtmx;
    double tst;
    char gm[256];
    FILE *ofd, *of2, *of3;

    dtmx = DATAMAX;

    *t = (double *)malloc((unsigned long)dtmx * sizeof(**t));
    *y12 = (double *)malloc((unsigned long)dtmx * sizeof(**y12));
    *y23 = (double *)malloc((unsigned long)dtmx * sizeof(**y23));
    *y31 = (double *)malloc((unsigned long)dtmx * sizeof(**y31));

    ofd = fopen(source1, "r");
    if (ofd == NULL)
        return (-1);
    else {
        do
            fgets(gm, 100, ofd);
        while ((gm[0] == '#') || (gm[0] == '%'));
        i = 0;
        nbv = sscanf(gm, "%lf %lf %lf", *t + i, *y12 + i, &tst);
        if (nbv != 2) {
            if (nbv != 1)
                nbv = -nbv;
            return (nbv);
        } else {
            do {
                i++;
                if (i >= dtmx) {
                    dtmx += DATAMAX;
                    if (dtmx > GRANMAX) {
                        printf(
                            "# File trucated to %ld "
                            "elements\n",
                            dtmx);
                        break;
                    }
                    *t = (double *)realloc(
                        *t,
                        (unsigned long)dtmx * sizeof(**t));
                    *y12 = (double *)realloc(
                        *y12,
                        (unsigned long)dtmx * sizeof(**y12));
                }
            } while (fscanf(ofd, "%lf %lf", *t + i, *y12 + i) == 2);
            fclose(ofd);
            n1 = i;
        }
    }

    dtmx = DATAMAX;
    of2 = fopen(source2, "r");
    if (of2 == NULL)
        return (-1);
    else {
        do
            fgets(gm, 100, of2);
        while ((gm[0] == '#') || (gm[0] == '%'));
        i = 0;
        nbv = sscanf(gm, "%lf %lf %lf", *t + i, *y23 + i, &tst);
        if (nbv != 2) {
            if (nbv != 1)
                nbv = -nbv;
            return (nbv);
        } else {
            do {
                i++;
                if (i >= dtmx) {
                    dtmx += DATAMAX;
                    if (dtmx > GRANMAX) {
                        printf(
                            "# File trucated to %ld "
                            "elements\n",
                            dtmx);
                        break;
                    }
                    *y23 = (double *)realloc(
                        *y23,
                        (unsigned long)dtmx * sizeof(**y23));
                }
            } while (fscanf(of2, "%lf %lf", *t + i, *y23 + i) == 2);
            fclose(of2);
            n2 = i;
        }
    }
    if (n1 == n2)
        N = n1;
    else {
        N = -2;
        return (N);
    }

    dtmx = DATAMAX;
    of3 = fopen(source3, "r");
    if (of3 == NULL)
        return (-1);
    else {
        do
            fgets(gm, 100, of3);
        while ((gm[0] == '#') || (gm[0] == '%'));
        i = 0;
        nbv = sscanf(gm, "%lf %lf %lf", *t + i, *y31 + i, &tst);
        if (nbv != 2) {
            if (nbv != 1)
                nbv = -nbv;
            return (nbv);
        } else {
            do {
                i++;
                if (i >= dtmx) {
                    dtmx += DATAMAX;
                    if (dtmx > GRANMAX) {
                        printf(
                            "# File trucated to %ld "
                            "elements\n",
                            dtmx);
                        break;
                    }
                    *y31 = (double *)realloc(
                        *y31,
                        (unsigned long)dtmx * sizeof(**y31));
                }
            } while (fscanf(of3, "%lf %lf", *t + i, *y31 + i) == 2);
            fclose(of3);
            n3 = i;
        }
    }
    if (n3 != N)
        N = -2;

    return (N);
}

/**
 * \brief          Load the two files pointed by 'source' and transfer
 *                 its content into the 'y' table.
 * \param *source  Path to data file.
 * \param **y      Pointer to array of sample value.
 * \return         -2 file with 2 columns
 *                 -1 file not found,
 *                 0 unrecognized file,
 *                 N length of the tables.
 */
long stio_load_1col(char *source, double **y) {
    long i, nbv, N;
    double tst;
    long dtmx;
    char gm[256];
    FILE *ofd;

    dtmx = DATAMAX;
    *y = (double *)malloc((unsigned long)dtmx * sizeof(**y));
    ofd = fopen(source, "r");
    if (ofd == NULL)
        return (-1);
    else {
        do
            fgets(gm, 100, ofd);
        while ((gm[0] == '#') || (gm[0] == '%'));
        i = 0;
        nbv = sscanf(gm, "%lf %lf %lf", *y + i, &tst, &tst);
        if (nbv != 1) {
            return (-nbv);
        } else {
            do {
                i++;
                if (i >= dtmx) {
                    dtmx += DATAMAX;
                    if (dtmx > GRANMAX) {
                        printf(
                            "# File trucated to %ld "
                            "elements\n",
                            dtmx);
                        break;
                    }
                    *y = (double *)realloc(*y,
                                           (unsigned long)dtmx * sizeof(**y));
                }
            } while (fscanf(ofd, "%lf", *y + i) == 1);
            fclose(ofd);
            N = i;
        }
    }
    return (N);
}

/**
 * \brief          Load the two files pointed by 'source' and transfer
 *                 its content into the 'y' table.
 * \param *source  Path to data file.
 * \param **y1     Pointer to array of sample value.
 * \param **y2     Pointer to array of sample value.
 * \return         -n file with 'n' columns
 *                 -1 file not found,
 *                 0 Memory allocation error,
 *                 N length of the tables.
 */
long stio_load_2col(char *source, double y1[128], double y2[128]) {
    int i, nbv, N;
    double tst;
    char gm[256];
    FILE *ofd;

    ofd = fopen(source, "r");
    if (ofd == NULL) {
        return (-1);
    }

    do {
        fgets(gm, 100, ofd);
    } while ((gm[0] == '#') || (gm[0] == '%'));

    i = 0;
    nbv = sscanf(gm, "%lf %lf %lf", &y1[i], &y2[i], &tst);
    if (nbv != 2) {
        return (-nbv);
    }

    do {
        i++;
        if (i >= 128) {
            printf("# File trucated to %d elements\n", i);
            break;
        }
    } while (fscanf(ofd, "%lf %lf", &y1[i], &y2[i]) == 2);

    fclose(ofd);
    N = i;

    return (N);
}

/**
 * \brief          Load the file pointed by 'source' and transfer its contain
 *                 into the serie structure.
 * \param *source
 * \param * serie
 * \return         -1 file not found
 *                 0 unrecognized file
 *                 N length of the tables
 */
int stio_load_adev(char *source, st_serie *serie) {
    int i, nbv, N;
    double tst, dev, tau;
    char gm[256];
    FILE *ofd;

    if (strlen(source) == 0 || (strlen(source) == 1 && source[0] == '-'))
        ofd = stdin;
    else
        ofd = fopen(source, "r");

    if (ofd == NULL)
        return (-1);
    else {
        // Ignore leading comment lines :
        do
            fgets(gm, 100, ofd);
        while ((gm[0] == '#') || (gm[0] == '%'));

        i = 0;
        nbv = sscanf(gm, "%lf %lf %lf", &tau, &dev, &tst);
        if (nbv < 2) {
            if (nbv != 1)
                nbv = -nbv;
            return (nbv);
        }

        serie->tau_base = tau;
        do {
            if ((gm[0] == '#') || (gm[0] == '%'))
                continue;
            if (sscanf(gm, "%lf %lf", &tau, &dev) >= 2) {
                serie->tau_norm[i] = (unsigned long)(tau / serie->tau_base);
                serie->dev[i].mean = dev;
                i++;
            }
        } while (fgets(gm, 100, ofd) != NULL);
        fclose(ofd);
        N = i;
        serie->length = N;
        return (N);
    }
}

/**
 * \brief          Load the file pointed by 'source' and transfer its contain
 *                 into the 'coeff' tables.
 * \param *source
 * \param coef
 * \return         -1 file not found
 *                 0 unrecognized file
 *                 N length of the tables
 */
int stio_load_coef(char *source, st_asymptote_coeff coeff) {
    int nbv;
    char gm[512], tst[256];
    FILE *ofd;

    ofd = fopen(source, "r");
    if (ofd == NULL)
        return (-1);

    do
        fgets(gm, 256, ofd);
    while ((gm[0] == '#') || (gm[0] == '%'));

    nbv = sscanf(gm, "%lf %lf %lf %lf %lf %lf %s", &coeff[0],
                 &coeff[1], &coeff[2], &coeff[3], &coeff[4],
                 &coeff[5], tst);
    fclose(ofd);
    if (nbv != 6)
        if (nbv != 1)
            nbv = -nbv;

    return (nbv);
}

/**
 * \brief          Load the file pointed by 'source' and transfer its contain
 *                 into the 'tau', 'adev' and 'ubad' tables.
 * \param *source
 * \param *serie
 * \return         -1 file not found
 *                 0 unrecognized file
 *                 N length of the tables
 */
int stio_load_adev_3col(char *source, st_serie *serie) {
    int i, nbv, N;
    double tst, dev, tau, ubad;
    char gm[256];
    FILE *ofd;

    ofd = fopen(source, "r");
    if (ofd == NULL)
        return (-1);

    do
        fgets(gm, 256, ofd);
    while ((gm[0] == '#') || (gm[0] == '%'));

    nbv = sscanf(gm, "%lf %lf %lf %lf", &tau, &dev, &ubad, &tst);
    if (nbv < 2) {
        if (nbv != 1)
            nbv = -nbv;
        return (nbv);
    }

    serie->tau_base = tau;

    i = 0;
    do {
        if ((gm[0] == '#') || (gm[0] == '%'))
            continue;
        nbv = sscanf(gm, "%lf %lf %lf %lf",
                     &tau, &dev, &ubad, &tst);
        if (nbv == 2)
            ubad = -1;
        if (nbv >= 2) {
            serie->tau_norm[i] = (unsigned long)(tau / serie->tau_base);
            serie->dev[i].mean = dev;
            serie->dev[i].unb = ubad;
            i++;
        }
    } while (fgets(gm, 256, ofd) != NULL);
    fclose(ofd);

    N = i;
    serie->length = N;

    return (N);
}

/**
 * \brief          Load the file pointed by 'source' and transfer its contain
 *                 into the 'tau', 'adev', 'ubad' and bounds (b1, b2, b3, b4)
 *                 tables.
 * \param *source
 * \param tau[]
 * \param adev[]
 * \param ubad[]
 * \param b1[]
 * \param b2[]
 * \param b3[]
 * \param b4[]
 * \return         -1 file not found
 *                 0 unrecognized file
 *                 N length of the tables
 */
int stio_load_adev_7col(char *source, st_serie *serie) {
    int i, nbv, N;
    char gm[256];
    double tau, dev, ubad, bmin2s, bmin1s, bmax1s, bmax2s;
    FILE *ofd;

    ofd = fopen(source, "r");
    if (ofd == NULL)
        return (-1);

    do
        fgets(gm, 256, ofd);
    while ((gm[0] == '#') || (gm[0] == '%'));

    i = 0;
    nbv = sscanf(gm, "%lf %lf %lf %lf %lf %lf %lf",
                 &tau, &dev, &ubad,
                 &bmin2s, &bmin1s, &bmax1s, &bmax2s);
    if (nbv < 7) {
        if (nbv != 1)
            nbv = -nbv;
        return (nbv);
    }

    serie->tau_base = tau;
    do {
        if ((gm[0] == '#') || (gm[0] == '%'))
            continue;
        if (sscanf(gm, "%lf %lf %lf %lf %lf %lf %lf",
                   &tau, &dev, &ubad,
                   &bmin2s, &bmin1s, &bmax1s, &bmax2s) >= 7) {
            serie->tau_norm[i] = (unsigned long)(tau / serie->tau_base);
            serie->dev[i].mean = dev;
            serie->dev[i].unb = ubad;
            serie->dev[i].bmin2s = bmin2s;
            serie->dev[i].bmin1s = bmin1s;
            serie->dev[i].bmax1s = bmax1s;
            serie->dev[i].bmax2s = bmax2s;
            i++;
        }
    } while (fgets(gm, 256, ofd) != NULL);
    fclose(ofd);

    N = i;
    serie->length = N;

    return (N);
}

/*  Adding the suffix to the file name.					    */
/*							    FV	1992/04/17  */
char *stu_mod_nom(char *nom_brut, char *suffixe) {
    int i, point;
    static char nomfich[64];
    point = 0;

    for (i = 0; i < 64; ++i) { /* Seeking the '.' in the file name. */
        if (nom_brut[i] == 0) {
            point = i;
            break;
        }
    }
    for (i = 0; i <= point; ++i)
        nomfich[i] = nom_brut[i];
    i = 0;
    do
        ++i;
    while ((nomfich[i] != 0) && (nomfich[i] != '.'));
    if (nomfich[i] == 0)
        nomfich[i] = '.';
    strcpy(&nomfich[i + 1], suffixe); /* Adding the suffix. */

    return (nomfich);
}