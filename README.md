Libsigmatheta - Sigma Theta library
===================================

This is libsigmatheta, the Sigma Theta library, a collection of numerical routines for Time and Frequency metrology computing.

Libsigmatheta is free software, you can redistribute it and/or modify it under the terms of the CeCILL license ("http://www.cecill.info").

This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

Copyright or © or Copr. Université de Franche-Comté, Besançon, France
Contributor: François Vernotte, UTINAM/OSU THETA (2012/07/17)
Contact: francois.vernotte@obs-besancon.fr
SigmaTheta, a collection of computer programs dedicated to time and frequency metrology. 

Copyright Benoit Dubois, 2024 (dubois dot benoit at gmail dot com)
Splits SigmaTheta programs into a library and cli programs.


Installation
============

Libsigmatheta follows the standard GNU installation procedure.

To compile SigmaTheta library you will need the gnu gcc C-compiler and the GSL library (Gnu Scientific Library, see http://www.gnu.org/software/gsl/).

The simplest way to compile this package is:

1. `cd' to the directory containing the package's source code and type `./configure' to configure the package for your system.

2. After unpacking the distribution the Makefiles will build the binaries using the make command:

	make

3. If you intend to place the binaries into the /usr/local/lib/ directory, tape as root:

	make install


Please consult the INSTALL file in this distribution for more detailed instructions.

