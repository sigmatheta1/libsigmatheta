#  LIB-SIGMA-THETA Project - Makefile for the SIGMA-THETA library
#
# Fork of Sigma Theta by Benoit Dubois, dubois.benoit@gmail.com
# Splits SigmaTheta programs into a library and cli programs.
#
# This software, libsigmatheta, is a collection of routines for
# time and frequency metrology.
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software. You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author, the holder of the
# economic rights,  and the successive licensors  have only limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading, using, modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and, more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.
#

# Uncomment one of the variable OPTIMIZE or DEBUG.
DEBUG = -ggdb
#OPTIMIZE = -O2

PREFIX = /usr/local

TARGETDIR = build
OBJDIR = obj
SRCDIR = source
# External library source directory
LIBDIR = source/lib

LIBNAME = libsigmatheta
MAJOR = 4
MINOR = 1
BUILD = 0

# ----------- Do not edit below -----------------------------------------------

CC = gcc
CFLAGS = $(DEBUG) -fPIC -c -Wall -Wconversion -Wfloat-conversion -Wextra $(OPTIMIZE)
LDFLAGS	= $(DEBUG) -shared -Wl,-soname,$(LIBNAME)
LDLIBS = -lgsl -lgslcblas -lm
FFTW3 = -lfftw3

VERSION = $(MAJOR).$(MINOR).$(BUILD)

TARGET := $(TARGETDIR)/$(LIBNAME).so

SRCS = $(wildcard $(SRCDIR)/*.c)
LIBS = $(wildcard $(LIBDIR)/*.c)
OBJS := $(subst $(SRCDIR)/,$(OBJDIR)/, $(SRCS:.c=.o))
DEPS := $(subst $(SRCDIR)/,$(OBJDIR)/, $(SRCS:.c=.d))
LIBOBJS = $(subst $(LIBDIR)/,$(OBJDIR)/, $(LIBS:.c=.o))
LIBDEPS = $(subst $(LIBDIR)/,$(OBJDIR)/, $(LIBS:.c=.d))

.PHONY: all
all: $(TARGET)

.SECONDEXPANSION:

$(TARGET): $(OBJS) $(LIBOBJS) | $$(@D)
	$(CC) $(LDFLAGS) -o $@ $^ $(LDLIBS) $(FFTW3)
	echo "libsigmatheta compiled"

$(OBJDIR)/%.o:$(SRCDIR)/%.c | $$(@D)
	$(COMPILE.c) $(OUTPUT_OPTION) $<

$(DEPS): $(OBJDIR)/%.d:$(SRCDIR)/%.c | $$(@D)
	$(CC) $(CFLAGS) -MM $< >$@

-include $(DEPS)

$(OBJDIR)/%.o:$(LIBDIR)/%.c| $$(@D)
	$(COMPILE.c) $(OUTPUT_OPTION) $<

$(LIBDEPS): $(OBJDIR)/%.d:$(LIBDIR)/%.c| $$(@D)
	$(CC) $(CFLAGS) -MM $< >$@

-include $(LIBDEPS)

$(TARGETDIR):
	mkdir -p $@

$(OBJDIR):
	mkdir -p $@


.ONESHELL:

.PHONY: install
install: $(DESTDIR)$(PREFIX)/lib/$(LIBNAME)

$(DESTDIR)$(PREFIX)/lib/$(LIBNAME): $(TARGET)
	mkdir -p $(DESTDIR)$(PREFIX)/lib
	cp $(TARGET) $@.so.$(MAJOR).$(MINOR).$(BUILD)
	cd $(DESTDIR)$(PREFIX)/lib
	ln -sf $(LIBNAME).so.$(MAJOR).$(MINOR).$(BUILD) $(LIBNAME).so.$(MAJOR).$(MINOR)
	ln -sf $(LIBNAME).so.$(MAJOR).$(MINOR) $(LIBNAME).so.$(MAJOR)
	ln -sf $(LIBNAME).so.$(MAJOR) $(LIBNAME).so
	ldconfig


uninstall:
	rm -f $(DESTDIR)$(PREFIX)/lib/$(LIBNAME)*


.PHONY: clean
clean:
	rm -f $(TARGET) $(OBJDIR)/*
